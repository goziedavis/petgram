from django.shortcuts import render, redirect
from .forms import PhotoForm
from .models import Photo
from django.shortcuts import HttpResponse


# Create your views here.
def index(request):
    photos = Photo.objects.all()
    return render(request, "Pets/frontpage.html", {'photos': photos})


def add_photo(request):
    if request.method == "POST":
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            photo = Photo()
            photo.image = request.FILES.get('image')
            photo.caption = request.POST.get('caption')
            photo.user = request.user
            photo.save()

            return redirect('/')
        else:
            return HttpResponse(form.errors)

    form = PhotoForm
    return render(request, "Pets/upload_image.html", {'form': form})
