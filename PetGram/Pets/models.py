from django.db import models
from cloudinary.models import CloudinaryField
from django.contrib.auth.models import User
from django.db.models.signals import pre_delete
from django.dispatch import receiver
import cloudinary.uploader


# Create your models here.
class Photo(models.Model):
    image = CloudinaryField('image')
    caption = models.CharField(max_length=100, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        if self.caption != "":
            return self.caption

        return "No Caption"


# @receiver(pre_delete, sender=Photo)
# def photo_delete(sender, instance, **kwargs):
#     cloudinary.uploader.destroy(instance.image.public_id)
