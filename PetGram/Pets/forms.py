# from django.forms import ModelForm
import django.forms as forms
from .models import Photo


class PhotoForm(forms.ModelForm):
    image = forms.FileField()
    caption = forms.CharField()

    class Meta:
        model = Photo
        fields = ('image', 'caption')
